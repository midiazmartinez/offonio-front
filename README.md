# OffOn.io App

Front-End construido con:

The Progressive JavaScript Framework [Vue.js](https://vuejs.org/).

## Empezando

Clonar el repo & ejecutar `npm install` desde la ra�z del proyecto

## Los comandos disponibles

```shell
npm start
```

Ejecuta el script para el servidor Express en producci�n

```shell
npm run lint:js
```

Lints javascript-files inside `/src` directory

```shell
npm run build
```

Ejecuta el webpack module-bundler con los ajustes de producci�n (comprimir etc.) y construye el proyecto en el directorio `/build`.

```shell
npm run dev
```

Runs the Webpack module-bundler, starts watching for changes & launches the BrowserSync server to [http://localhost:5000](http://localhost:5000) (it's possible to change the port from `package.json` config-section).

**Note!** Webpack handles all the reloading stuff while BrowserSync just proxies the default webpack-port (`8080`) giving the possibility to connect to dev-server from multiple devices.