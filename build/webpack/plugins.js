const argv = require('minimist')(process.argv.slice(2));
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const DashboardPlugin = require('webpack-dashboard/plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const imageminMozjpeg = require('imagemin-mozjpeg');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const path = require('path');
const webpack = require('webpack');
const WebpackCleanupPlugin = require('webpack-cleanup-plugin');
const WebpackNotifierPlugin = require('webpack-notifier');
const pkg = require('../../package.json');

const isProduction = !!((argv.env && argv.env.production) || argv.p);
const resolve = dir => path.join(__dirname, '../../', dir);

/**
 * Common plugins
 * @type {*[]}
 */
const commonPlugins = [
  new HtmlWebpackPlugin({
    inject: 'body',
    template: resolve('src/index.html'),
    favicon: resolve('src/app/assets/images/favicon.ico')
  }),
  new webpack.LoaderOptionsPlugin({
    minimize: isProduction,
    debug: !isProduction,
    stats: { colors: true },
    eslint: {
      configFile: resolve('.eslintrc'),
      failOnWarning: false,
      failOnError: true,
    },
  }),
  new webpack.ProvidePlugin({
    $: 'jquery',
    jquery: 'jquery',
    'window.jQuery': 'jquery',
    jQuery: 'jquery'
  }),
  new ImageminPlugin({
    disable: false,
    optipng: {
      optimizationLevel: 7,
    },
    gifsicle: {
      optimizationLevel: 3,
    },
    pngquant: {
      quality: '65-90',
      speed: 4,
    },
    svgo: {
      removeUnknownsAndDefaults: false,
      cleanupIDs: false,
    },
    jpegtran: null,
    plugins: [imageminMozjpeg({
      quality: 75,
    })]
  }),
  new ExtractTextPlugin({
    filename: 'css/[name].[hash].css',
    allChunks: true,
  }),
];

/**
 * Develop plugins
 * @type {Array.<*>}
 */
const developPlugins = [
  new webpack.DefinePlugin({
    'process.env.APP_NAME': '"offon.io"',
    'process.env.NODE_ENV': '"development"',
    'process.env.API_HOST': '"http://localhost:3000"',
    'process.env.API_VERSION': '"v1"'
  }),
  new DashboardPlugin(),
  new webpack.NamedModulesPlugin(),
  new WebpackNotifierPlugin({
    title: pkg.name,
    contentImage: path.join(__dirname, '../../src/assets/images/logo.png')
  }),
  new BrowserSyncPlugin({
    host: 'localhost',
    port: pkg.config.port,
    // Proxy the default webpack dev-server port
    proxy: 'http://localhost:8080/',
    notify: false,
    open: false,
    // Let webpack handle the reload
    codeSync: false
  }),
  new webpack.optimize.OccurrenceOrderPlugin(),
  new webpack.NoEmitOnErrorsPlugin()
];

/**
 * Production plugins
 * @type {Array.<*>}
 */
const productionPLugins = [
  new webpack.DefinePlugin({
    'process.env.APP_NAME': '"offon.io"',
    'process.env.NODE_ENV': '"production"',
    'process.env.API_HOST': '"https://api.offon.io"',
    'process.env.API_VERSION': '"v1"'
  }),
  new WebpackCleanupPlugin(),
  new webpack.optimize.OccurrenceOrderPlugin(),
  new webpack.optimize.UglifyJsPlugin({
    compressor: {
      drop_console: true,
      warnings: false
    }
  }),
  new OptimizeCssAssetsPlugin({
    cssProcessorOptions: {
      discardComments: {
        removeAll: true
      }
    }
  }),
  new FaviconsWebpackPlugin({
    title: `${pkg.name} - ${pkg.description}`,
    logo: resolve('src/app/assets/images/favicon.png'),
    prefix: 'img/icons/',
    statsFilename: 'iconstats-[hash].json',
    icons: {
      android: true, // Create Android homescreen icon.
      appleIcon: true, // Create Apple touch icons.
      appleStartup: false, // Create Apple startup images.
      coast: { offset: 25 }, // Create Opera Coast icon with offset 25%.
      favicons: true, // Create regular favicons.
      firefox: true, // Create Firefox OS icons.
      windows: true, // Create Windows 8 tile icons.
      yandex: true // Create Yandex browser icon.
    }
  })
];

module.exports = {
  develop: commonPlugins.concat(developPlugins),
  production: commonPlugins.concat(productionPLugins)
};
