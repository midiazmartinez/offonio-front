const path = require('path');
const argv = require('minimist')(process.argv.slice(2));
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const isProduction = !!((argv.env && argv.env.production) || argv.p);
const resolve = dir => path.join(__dirname, '../../', dir);

module.exports = {
  entry: {
    app: resolve('src/main.js')
  },
  output: {
    path: resolve('dist'),
    publicPath: '/',
    filename: 'js/[name].[hash].js',
    sourceMapFilename: 'js/[name].[hash].js.map',
    chunkFilename: 'js/[id].chunk.js',
  },
  resolve: {
    extensions: ['*', '.js', '.vue', 'html'],
    alias: {
      src: resolve('src'),
      assets: resolve('src/app/assets'),
      components: resolve('src/app/components'),
      layoutpublicclient: resolve('src/app/pages/public/client'),
      layoutpublicmain: resolve('src/app/pages/public/main'),
      viewspublic: resolve('src/app/pages/public/views'),
      layoutadmin: resolve('src/app/pages/private/main'),
      viewsadmin: resolve('src/app/pages/private/views'),
      services: resolve('src/app/services'),
      store: resolve('src/app/store'),
      api: resolve('src/app/services/api'),
      locales: resolve('src/app/locales'),
      styles: resolve('src/app/styles'),
      enums: resolve('src/app/enums'),
      'router-app': resolve('src/app/router'),
      boot: resolve('src/boot'),
      endpoints: resolve('src/app/enums/endpoints.js'),
      libs: resolve('src/app/vendors/js'),
      vue$: isProduction ? 'vue/dist/vue.min.js' : 'vue/dist/vue.js'
    }
  },
  devServer: {
    historyApiFallback: true,
    inline: true,
    contentBase: resolve('src'),
    compress: true
  },
  module: {
    rules: [
      {
        enforce: 'pre',
        exclude: /node_modules/,
        loader: 'eslint-loader',
        test: /\.js?$/
      },
      {
        exclude: [/(node_modules)(?![/|\\](bootstrap|foundation-sites))/],
        test: /\.js$/,
        use: [{
          loader: 'babel-loader',
          query: {
            cacheDirectory: true
          }
        }]
      },
      {
        exclude: /node_modules/,
        loader: 'vue-loader',
        test: /\.vue$/
      },
      {
        exclude: /node_modules/,
        loader: 'html-loader',
        test: /\.html$/
      },
      {
        test: /\.css$/,
        loader: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'cache-loader'
            },
            {
              loader: 'css-loader',
              options: {
                sourceMap: !isProduction
              }
            }],
        })),
      },
      {
        test: /\.(sass|scss)$/,
        include: resolve('src'),
        loader: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                sourceMap: !isProduction
              }
            },
            {
              loader: 'resolve-url-loader',
              options: {
                sourceMap: !isProduction
              }
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: !isProduction
              }
            }
          ],
        }))
      },
      {
        test: /\.(png|jpe?g|gif|svg|xml|json)$/,
        include: resolve('src'),
        use: {
          loader: 'file-loader',
          options: {
            name: 'img/[name].[ext]',
          },
        },
      },
      {
        test: /\.(ttf|eot)$/,
        include: resolve('src'),
        use: {
          loader: 'file-loader',
          options: {
            name: 'fonts/[name].[ext]',
          },
        },
      },
      {
        test: /\.woff2?$/,
        include: resolve('src'),
        use: {
          loader: 'url-loader',
          options: {
            limit: 10000,
            mimetype: 'application/font-woff',
            name: 'fonts/[name].[ext]',
          },
        },
      },
      {
        test: /\.(ttf|eot|woff2?|png|jpe?g|gif|svg)$/,
        include: /node_modules/,
        use: {
          loader: 'file-loader',
          query: {
            name: 'fonts/[name].[ext]'
          },
        },
      }
    ]
  }
};
