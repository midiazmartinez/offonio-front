const path = require('path');
const express = require('express');

const app = express();
const distPath = path.join(__dirname, '../', 'dist');
const port = process.env.PORT || 8080;

const httpsRedirect = (req, res, next) => {
  if (req.headers['x-forwarded-proto'] === 'https') return next();
  return res.redirect(301, `https://${req.hostname}${req.url}`);
};

if (process.env.NODE_ENV === 'production') {
  app.use(httpsRedirect);
}

app.use(express.static(distPath));
app.get('*', (req, res) => res.sendFile(`${distPath}/index.html`));
app.listen(port, () => console.log(`App is running on port ${port}`));
