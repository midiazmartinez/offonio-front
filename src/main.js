import { Vue, router, i18n, store } from 'boot/initial';

new Vue({ router, i18n, store }).$mount('#app');
