import Vue from 'vue';
import axios from 'axios';
import NProgress from 'nprogress';
import auth from 'services/auth';
import _ from 'lodash';

axios.defaults.baseURL = `${process.env.API_HOST}/${process.env.API_VERSION}`;

axios.interceptors.request.use(config => {
  NProgress.start();
  config.headers.Authorization = `JWT ${localStorage.getItem('token')}`;
  return config;
}, error => Promise.reject(error.data));

axios.interceptors.response.use(
  response => {
    NProgress.done();
    return Promise.resolve(response.data);
  },
  error => {
    NProgress.done();
    const errorData = error.response.data;
    if (error.response.status === 500 && errorData) {
      if (_.has(errorData.data, 'expiredAt')) {
        auth.logout();
      }
    }
    return Promise.reject(errorData);
  }
);

Vue.prototype.$http = axios;
