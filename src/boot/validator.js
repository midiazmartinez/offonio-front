import Vue from 'vue';
import VueValidate from 'vee-validate';
import en from 'locales/en';
import es from 'locales/es';

VueValidate.Validator.extend('user_and_email', value => VueValidate.Rules.email(value) || VueValidate.Rules.alpha_dash(value));

Vue.use(VueValidate, {
  locale: localStorage.getItem('lang') || 'en',
  dictionary: { // dictionary object
    en: en.validator,
    es: es.validator
  }
});
