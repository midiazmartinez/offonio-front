import Vue from 'vue';
import store from 'store';
import auth from 'services/auth';
import 'bootstrap-sass'; // Bootstrap 3.X
import 'styles/main.sass';

// Global registry components
import 'components/Language/language';

import router from './router';
import i18n from './locale';
import './http';
import './validator';
import './directive';

// Check the user's auth status when the app starts
auth.checkAuth();

export { Vue, router, i18n, store };
