import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from 'router-app';
import policies from 'services/policies';
import i18n from './locale';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  linkActiveClass: 'active',
  hashbang: false,
  root: '/',
  saveScrollPosition: true,
  routes
});

router.beforeEach((to, from, next) => {
  document.title = `${i18n.t(`title.${to.meta.titleKey}`)} | ${process.env.APP_NAME}`;
  if (to.matched.some(m => m.meta.super)) {
    policies.isSuper(to, from, next);
  } else if (to.matched.some(m => m.meta.admin)) {
    policies.isAdmin(to, from, next);
  } else if (to.matched.some(m => m.meta.guest)) {
    policies.isGuest(to, from, next);
  } else if (to.matched.some(m => m.meta.auth)) {
    policies.isAuthenticated(to, from, next);
  } else if (to.matched.some(m => m.meta.notauth)) {
    policies.isNotAuthenticated(to, from, next);
  } else if (to.matched.some(m => m.meta.entry)) {
    policies.isEntry(to, from, next);
  } else {
    next();
  }
});

export { router as default };
