import Vue from 'vue';
import Croppa from 'vue-croppa';
import { Avatar } from 'vue-avatar';
import VueStrap from 'vue-strap';
import endpoint from 'endpoints';
import template from './uploadAvatar.html';

export default Vue.component('upload-avatar', {
  template,

  components: {
    avatar: Avatar,
    modal: VueStrap.modal
  },

  created() {
    Vue.use(Croppa);
  },

  props: {
    fullName: { type: String, required: true },
    avatarUrl: { type: String, default: '' }
  },

  data() {
    return {
      croppaProfile: {},
      showHoverAvatar: false,
      showCroppaAvatar: false,
      hasError: false,
      errorText: '',
      avatar: null
    };
  },

  watch: {
    avatarUrl(newVal) {
      this.avatar = newVal;
    }
  },

  computed: {
    hasImage() {
      return !!this.avatar;
    },

    hasImageCroppa() {
      return this.croppaProfile.hasImage && this.croppaProfile.hasImage();
    }
  },

  methods: {
    closeModal() {
      this.showCroppaAvatar = false;
    },

    confirmUploadAvatar() {
      if (this.croppaProfile.hasImage()) {
        const croppaFile = this.croppaProfile.getChosenFile();
        this.croppaProfile.generateBlob(blob => {
          const data = new FormData();
          data.append('avatar', blob, croppaFile.name);
          this.$http.post(endpoint.AVATAR, data, {
            headers: { 'content-type': 'multipart/form-data' }
          }).then(response => {
            if (response.code === 'OK') {
              console.log(response);
            }
          }, error => {
            console.log(error);
          });
        }, croppaFile.type);
        this.avatar = this.croppaProfile.generateDataUrl();
      }

      this.closeModalAvatar();
    },

    closeModalAvatar() {
      this.hasError = false;
      this.croppaProfile.remove();
      this.closeModal();
    },

    removeImage() {
      this.avatar = null;
    },

    onFileTypeMismatch() {
      this.hasError = true;
      this.errorText = this.$t('error.fileTypeMismatch');
    },

    onFileSizeExceed() {
      this.hasError = true;
      this.errorText = this.$t('error.fileSizeExceed');
    }
  }
});
