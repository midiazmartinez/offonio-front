import Vue from 'vue';
import template from './shareButtons.html';

export default Vue.component('share-buttons', {
  template,

  props: {
    facebook: { type: String },
    twitter: { type: String },
    linkedin: { type: String },
    target: { type: String, default: '_blank' }
  },

  data() {
    return {};
  },

  methods: {
  }
});
