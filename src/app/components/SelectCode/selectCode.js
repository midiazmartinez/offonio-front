import Vue from 'vue';
import vSelect from 'vue-select';
import _ from 'lodash';
import template from './selectCode.html';

export default Vue.component('select-code', {
  template,

  components: {
    vSelect
  },

  props: {
    omitOptions: Array,
    valuesOptions: { type: String, default: '0123456789ABCDEFGHIJKLMNOPQRSTVWXYZ' },
    value: String,
    showPlaceholder: { type: Boolean, default: false }
  },

  data() {
    return {
    };
  },

  computed: {
    customOptions() {
      return _.xor(this.valuesOptions.split(''), this.omitOptions);
    },

    getPlaceholder() {
      return this.showPlaceholder ? this.$t('ui.placeholder.selectCode') : '';
    }
  },

  methods: {
    changeCallback(val) {
      this.$emit('change-code', val);
    }
  },
});
