import Vue from 'vue';
import _ from 'lodash';
import VueStrap from 'vue-strap';
import { ClientTable } from 'vue-tables-2';
import template from './tableOffonio.html';

Vue.use(ClientTable, {}, false);

export default Vue.component('table-offonio', {
  template,

  components: {
    modal: VueStrap.modal
  },

  props: {
    records: { type: Array, required: true },
    columns: { type: Array, required: true },
    options: { type: Object, default() { return {}; } },
    confirmField: { type: String, default: 'id' },
    actions: {
      type: Object,
      default() {
        return {
          edit: true,
          delete: true
        };
      }
    }
  },

  data() {
    return {
      textConfirmDelete: '',
      showConfirmDelete: false,
      recordDeleteId: null,
      tableOptions: _.extend(this.options, {
        skin: 'table-striped table-bordered',
        sortable: _.without(this.columns, 'actions'),
        highlightMatches: true,
        perPage: 10,
        perPageValues: [10, 25, 50, 100],
        texts: {
          filter: '',
          limit: '',
          count: this.$t('ui.table.count'),
          filterPlaceholder: this.$t('ui.table.filterPlaceholder'),
          noResults: this.$t('ui.table.noResults')
        }
      })
    };
  },

  methods: {
    createEditUrl(id) {
      return `${this.$router.currentRoute.path}/${id}/edit`;
    },

    deleteItem(row) {
      this.showConfirmDelete = true;
      this.recordDeleteId = row.id;
      this.recordDeleteField = row[this.confirmField];
      this.textConfirmDelete = this.$t('ui.modal.contentConfirmDelete', { record: this.recordDeleteField });
    },

    confirmDelete() {
      this.$emit('delete-record', this.recordDeleteId, this.recordDeleteField);
      this.showConfirmDelete = false;
    }
  }
});
