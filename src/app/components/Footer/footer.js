import Vue from 'vue';
import 'components/ShareButtons/shareButtons';
import template from './footer.html';

export default Vue.component('footer-public', {
  template,

  props: {
    fixed: { type: Boolean, default: false }
  },

  data() {
    return {};
  }
});
