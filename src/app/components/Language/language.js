import Vue from 'vue';
import template from './language.html';

export default Vue.component('select-language', {
  template,
  data() {
    return {
      langs: ['en', 'es']
    };
  },

  methods: {
    changeLanguage(lang) {
      if (this.$i18n.locale !== lang) {
        localStorage.setItem('lang', lang);
        // this.$router.go(); // BUG: In Safari not working
        window.location.href = ''; // Reload page
      }
    }
  }
});
