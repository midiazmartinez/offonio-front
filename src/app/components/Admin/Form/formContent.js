import Vue from 'vue';
import router from 'boot/router';
import template from './formContent.html';

export default Vue.component('form-admin-content', {
  template,

  props: {
    textSubmit: { type: String, required: true },
    textCancel: { type: String, required: true }
  },

  data() {
    return {};
  },

  created() {
    this.$parent.$on('btn-submit-loading', this.loadingSubmit);
  },

  methods: {
    submitForm() {
      this.$emit('submit-form');
    },

    loadingSubmit(value) {
      if (value) {
        $(this.$refs.btnSubmit).button('loading');
      } else {
        $(this.$refs.btnSubmit).button('reset');
      }
    },

    onBack() {
      router.go(-1);
    }
  }
});
