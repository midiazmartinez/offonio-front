import Vue from 'vue';
import router from 'boot/router';
import template from './headerContent.html';

export default Vue.component('header-admin-content', {
  template,

  props: {
    title: String,
    subTitle: String,
    hasBackLink: Boolean
  },

  data() {
    return {};
  },

  methods: {
    onBack() {
      router.go(-1);
    }
  }
});
