import Vue from 'vue';
import 'components/Admin/HeaderContent/headerContent';
import template from './content.html';

export default Vue.component('admin-content', {
  template,

  props: {
    title: String,
    subTitle: String,
    hasBackLink: {
      type: Boolean,
      default: false
    }
  },

  created() {
    this.$parent.$on('btn-submit-loading', this.loadingSubmit);
  },

  data() {
    return {
    };
  },

  methods: {
    loadingSubmit(value) {
      this.$emit('btn-submit-loading', value);
    }
  }
});
