import Vue from 'vue';
import Vuex from 'vuex';
import getters from './getters';
import actions from './actions';
import mutations from './mutations';

Vue.use(Vuex);

const state = {
  advertiser: localStorage.getItem('advertiser') === 'true'
};

const debug = process.env.NODE_ENV !== 'production';
const store = new Vuex.Store({
  state,
  mutations,
  actions,
  getters,
  modules: {},
  strict: debug
});

export { store as default };
