import types from './mutation-types';

export default {
  toggleAdvertiser: ({ commit }) => commit(types.TOGGLE_ADVERTISER)
};
