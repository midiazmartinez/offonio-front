import types from './mutation-types';

export default {
  [types.TOGGLE_ADVERTISER]: state => {
    const advertiserChange = !state.advertiser;
    localStorage.setItem('advertiser', advertiserChange);
    state.advertiser = advertiserChange;
  }
};
