import Vue from 'vue';
import AdminHeader from 'layoutadmin/Header/header';
import 'components/Admin/Content/content';
import 'components/Table/tableOffonio';
import 'components/Admin/Form/formContent';
import ToggleButton from 'vue-js-toggle-button';
import Notifications from 'vue-notification';
import template from './layout.html';

Vue.use(ToggleButton);
Vue.use(Notifications);

export default Vue.extend({

  template,

  components: {
    AdminHeader
  }
});
