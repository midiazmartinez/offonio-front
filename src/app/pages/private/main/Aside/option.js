import Vue from 'vue';
import template from './option.html';

export default Vue.extend({
  template,

  props: {
    url: { type: String, required: true },
    text: { type: String, required: true }
  },

  data() {
    return {};
  },

  methods: {
    closeMenu() {
      const btnCollapse = $('[data-toggle="collapse"]');
      if (btnCollapse.is(':visible')) {
        btnCollapse.click();
      }
    }
  }
});
