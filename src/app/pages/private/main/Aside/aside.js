import Vue from 'vue';
import auth from 'services/auth';
import AdminAsideOption from 'layoutadmin/Aside/option';
import 'metismenu';
import template from './aside.html';

export default Vue.extend({
  template,

  mounted() {
    Vue.nextTick(() => {
      $('#side-menu').metisMenu();
    });
  },

  components: {
    AdminAsideOption
  },

  data() {
    return {
      user: auth.user,
      search: ''
    };
  },

  methods: {
    logout() {
      auth.logout();
    },

    onSearch() {
      this.$router.push({ name: 'admin-search', params: { search: this.search } });
      this.search = '';
      this.closeMenuMobile();
    },

    closeMenuMobile() {
      const btnCollapse = $('[data-toggle="collapse"]');
      if (btnCollapse.is(':visible')) {
        btnCollapse.click();
      }
    }
  }
});
