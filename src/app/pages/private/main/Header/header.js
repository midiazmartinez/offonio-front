import Vue from 'vue';
import AdminAside from 'layoutadmin/Aside/aside';
import AdminAuthenticated from 'layoutadmin/Authenticated/authenticated';
import 'libs/sb-admin.js';
import template from './header.html';

export default Vue.extend({

  template,

  data() {
    return {
      isCollapsed: true
    };
  },

  components: {
    AdminAside,
    AdminAuthenticated
  }
});
