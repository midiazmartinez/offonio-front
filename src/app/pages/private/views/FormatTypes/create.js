import Vue from 'vue';
import endpoint from 'endpoints';
import router from 'boot/router';
import _ from 'lodash';
import SelectCode from 'components/SelectCode/selectCode';
import template from './create.html';

export default Vue.extend({
  template,

  components: {
    SelectCode
  },

  data() {
    return {
      format: {
        name: '',
        code: '',
        description: ''
      },
      withoutCodes: []
    };
  },

  created() {
    this.$validator.locale = this.$i18n.locale;
    this.fetchData();
  },

  methods: {

    fetchData() {
      this.$http.get(`${endpoint.FORMAT_TYPES}?fields=code`)
        .then(codes => {
          this.withoutCodes = _.map(codes.data, 'code');
        }, () => {
          this.$notify({
            type: 'error',
            text: this.$t('ui.notify.error')
          });
        });
    },

    createFormat() {
      this.$validator.validateAll().then(result => {
        if (result) {
          this.$http.post(endpoint.FORMAT_TYPES, this.format).then(response => {
            if (response.code === 'CREATED') {
              this.$notify({
                type: 'success',
                text: this.$t('ui.notify.successCreate', { record: this.format.name })
              });
              this.goToFormatTypes();
            }
          }, () => {
            this.$notify({
              type: 'error',
              text: this.$t('ui.notify.errorCreate', { record: this.format.name })
            });
            this.goToFormatTypes();
          });
        }
      });
    },

    goToFormatTypes() {
      router.push({ name: 'format-types' });
    }
  },
});
