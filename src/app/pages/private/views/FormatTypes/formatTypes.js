import Vue from 'vue';
import endpoint from 'endpoints';
import _ from 'lodash';
import template from './formatTypes.html';

export default Vue.extend({
  template,

  data() {
    return {
      error: '',
      records: [],
      columns: ['name', 'code', 'description', 'active', 'actions'],
      options: {
        headings: {
          name: this.$t('ui.table.fields.name'),
          code: this.$t('ui.table.fields.code'),
          description: this.$t('ui.table.fields.description'),
          active: this.$t('ui.table.fields.active'),
          actions: this.$t('ui.table.fields.actions')
        },
        columnsDisplay: {
          description: 'not_mobile',
          code: 'not_mobile'
        }
      }
    };
  },

  created() {
    this.getFormatTypes();
  },

  methods: {
    getFormatTypes() {
      this.$http.get(`${endpoint.FORMAT_TYPES}?sort=name`).then(response => {
        this.records = _.map(response.data, item => {
          item.active = this.$t(`ui.table.fields.${item.active ? 'yes' : 'no'}`);
          return item;
        });
      }, error => {
        this.error = error.message;
      });
    },

    onDeleteRecord(id, field) {
      this.$http.delete(`${endpoint.FORMAT_TYPES}/${id}`).then(() => {
        this.$notify({
          type: 'success',
          text: this.$t('ui.notify.successDelete', { record: field })
        });
        this.getFormatTypes();
      }, () => {
        this.$notify({
          type: 'error',
          text: this.$t('ui.notify.errorDelete', { record: field })
        });
      });
    }
  },
});
