import Vue from 'vue';
import endpoint from 'endpoints';
import _ from 'lodash';
import router from 'boot/router';
import template from './edit.html';

export default Vue.extend({
  template,

  data() {
    return {
      id: this.$route.params.id,
      format: {
        name: '',
        code: null,
        description: '',
        active: false
      },
      withoutCodes: []
    };
  },

  created() {
    this.$validator.locale = this.$i18n.locale;
    this.fetchData();
  },

  methods: {

    fetchData() {
      this.$http.all([
        this.$http.get(`${endpoint.FORMAT_TYPES}/${this.id}`),
        this.$http.get(`${endpoint.FORMAT_TYPES}?fields=code`)
      ])
        .then(this.$http.spread((format, codes) => {
          this.format = _.omit(format.data, ['createdAt', 'updatedAt']);
          this.withoutCodes = _.without(_.map(codes.data, 'code'), this.format.code);
        }, () => {
          this.$notify({
            type: 'error',
            text: this.$t('ui.notify.error')
          });
        }));
    },

    updateFormat() {
      this.$validator.validateAll().then(result => {
        if (result) {
          this.$emit('btn-submit-loading', true);
          this.$http.put(`${endpoint.FORMAT_TYPES}/${this.id}`, this.format).then(() => {
            this.$emit('btn-submit-loading', false);
            this.$notify({
              type: 'success',
              text: this.$t('ui.notify.successEdit', { record: this.format.name })
            });
            this.goToFormatTypes();
          }, () => {
            this.$notify({
              type: 'error',
              text: this.$t('ui.notify.errorEdit', { record: this.format.name })
            });
            this.goToFormatTypes();
          });
        }
      });
    },

    goToFormatTypes() {
      router.push({ name: 'format-types' });
    }
  },
});
