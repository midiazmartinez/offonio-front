import Vue from 'vue';
import endpoint from 'endpoints';
import _ from 'lodash';
import router from 'boot/router';
import template from './edit.html';

export default Vue.extend({
  template,

  data() {
    return {
      id: this.$route.params.id,
      content: {
        name: '',
        code: null,
        description: '',
        active: false
      },
      withoutCodes: []
    };
  },

  created() {
    this.$validator.locale = this.$i18n.locale;
    this.fetchData();
  },

  methods: {

    fetchData() {
      this.$http.all([
        this.$http.get(`${endpoint.CONTENT_TYPES}/${this.id}`),
        this.$http.get(`${endpoint.CONTENT_TYPES}?fields=code`)
      ])
        .then(this.$http.spread((content, codes) => {
          this.content = _.omit(content.data, ['createdAt', 'updatedAt']);
          this.withoutCodes = _.without(_.map(codes.data, 'code'), this.content.code);
        }, () => {
          this.$notify({
            type: 'error',
            text: this.$t('ui.notify.error')
          });
        }));
    },

    updateContent() {
      this.$validator.validateAll().then(result => {
        if (result) {
          this.$emit('btn-submit-loading', true);
          this.$http.put(`${endpoint.CONTENT_TYPES}/${this.id}`, this.content).then(() => {
            this.$emit('btn-submit-loading', false);
            this.$notify({
              type: 'success',
              text: this.$t('ui.notify.successEdit', { record: this.content.name })
            });
            this.goToContentTypes();
          }, () => {
            this.$notify({
              type: 'error',
              text: this.$t('ui.notify.errorEdit', { record: this.content.name })
            });
            this.goToContentTypes();
          });
        }
      });
    },

    goToContentTypes() {
      router.push({ name: 'content-types' });
    }
  },
});
