import Vue from 'vue';
import endpoint from 'endpoints';
import router from 'boot/router';
import _ from 'lodash';
import SelectCode from 'components/SelectCode/selectCode';
import template from './create.html';

export default Vue.extend({
  template,

  components: {
    SelectCode
  },

  data() {
    return {
      content: {
        name: '',
        code: '',
        description: ''
      },
      withoutCodes: []
    };
  },

  created() {
    this.$validator.locale = this.$i18n.locale;
    this.fetchData();
  },

  methods: {

    fetchData() {
      this.$http.get(`${endpoint.CONTENT_TYPES}?fields=code`)
        .then(codes => {
          this.withoutCodes = _.map(codes.data, 'code');
        }, () => {
          this.$notify({
            type: 'error',
            text: this.$t('ui.notify.error')
          });
        });
    },

    createContent() {
      this.$validator.validateAll().then(result => {
        if (result) {
          this.$http.post(endpoint.CONTENT_TYPES, this.content).then(response => {
            if (response.code === 'CREATED') {
              this.$notify({
                type: 'success',
                text: this.$t('ui.notify.successCreate', { record: this.content.name })
              });
              this.goToContentTypes();
            }
          }, () => {
            this.$notify({
              type: 'error',
              text: this.$t('ui.notify.errorCreate', { record: this.content.name })
            });
            this.goToContentTypes();
          });
        }
      });
    },

    goToContentTypes() {
      router.push({ name: 'content-types' });
    }
  },
});
