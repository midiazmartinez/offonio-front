import Vue from 'vue';
import endpoint from 'endpoints';
import _ from 'lodash';
import template from './users.html';

export default Vue.extend({
  template,

  data() {
    return {
      error: '',
      records: [],
      columns: ['fullName', 'username', 'email', 'rolesNames', 'active', 'actions'],
      actions: { edit: true },
      options: {
        headings: {
          fullName: this.$t('ui.table.fields.fullName'),
          username: this.$t('ui.table.fields.username'),
          email: this.$t('ui.table.fields.email'),
          rolesNames: this.$t('ui.table.fields.roles'),
          active: this.$t('ui.table.fields.active'),
          actions: this.$t('ui.table.fields.actions')
        },
        columnsDisplay: {
          username: 'not_mobile',
          email: 'not_mobile',
          rolesNames: 'not_mobile'
        }
      }
    };
  },

  created() {
    this.getUsers();
  },

  methods: {
    getUsers() {
      this.$http.get(endpoint.USERS_FULL).then(response => {
        this.records = _.map(response.data, item => {
          item.fullName = item.person.fullName;
          item.active = this.$t(`ui.table.fields.${item.active ? 'yes' : 'no'}`);
          item.rolesNames = _.join(_.map(item.roles, 'name'), ' / ');
          return item;
        });
      }, error => {
        this.error = error.message;
      });
    }
  },
});
