import Vue from 'vue';
import endpoint from 'endpoints';
import vSelect from 'vue-select';
import _ from 'lodash';
import router from 'boot/router';
import template from './edit.html';

export default Vue.extend({
  template,

  components: {
    vSelect
  },

  data() {
    return {
      error: '',
      id: this.$route.params.id,
      selected: null,
      roles: [],
      user: {
        username: '',
        password: '',
        email: '',
        active: false,
        person: {
          name: ''
        }
      },
    };
  },

  created() {
    this.$validator.locale = this.$i18n.locale;
    this.fetchData();
  },

  methods: {

    fetchData() {
      this.$http.all([this.getUser(), this.getRoles()])
        .then(this.$http.spread((user, roles) => {
          this.user = _.omit(user.data, ['createdAt', 'updatedAt']);
          this.roles = roles.data;
          this.selected = this.getSelectedRoles(user.data.roles);
        }, error => {
          this.error = error.message;
        }));
    },

    getUser() {
      return this.$http.get(`${endpoint.USERS_FULL}/${this.id}`);
    },

    getRoles() {
      return this.$http.get(endpoint.ROLES);
    },

    getSelectedRoles(roles) {
      const result = [];
      _.each(roles, rol => {
        result.push(_.pick(rol, ['id', 'name']));
      });
      return result;
    },

    updateUser() {
      this.$validator.validateAll().then(result => {
        if (result) {
          this.$emit('btn-submit-loading', true);
          this.$http.put(`${endpoint.USERS}/${this.id}`, {
            active: this.user.active,
            roles: this.selected
          }).then(response => {
            this.$emit('btn-submit-loading', false);
            if (response.code === 'OK') {
              this.$notify({
                type: 'success',
                text: this.$t('ui.notify.successEdit', { record: this.user.person.name })
              });
              this.goToUsers();
            }
          }, error => {
            this.error = error.message;
            this.$notify({
              type: 'error',
              text: this.$t('ui.notify.errorEdit', { record: this.user.person.name })
            });
            this.goToUsers();
          });
        }
      });
    },

    goToUsers() {
      router.push({ name: 'admin-users' });
    }
  },
});
