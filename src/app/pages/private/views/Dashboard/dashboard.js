import Vue from 'vue';
import template from './dashboard.html';

export default Vue.extend({
  template,

  data() {
    return {
      error: '',
    };
  },

  created() {
  },

  methods: {
  },
});
