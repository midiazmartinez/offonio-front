import Vue from 'vue';
import template from './create.html';

export default Vue.extend({
  template,

  data() {
    return {
      error: '',
    };
  },

  created() {
  },

  methods: {
  },
});
