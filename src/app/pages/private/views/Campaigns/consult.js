import Vue from 'vue';
import template from './consult.html';

export default Vue.extend({
  template,

  data() {
    return {
      error: '',
    };
  },

  created() {
  },

  methods: {
  },
});
