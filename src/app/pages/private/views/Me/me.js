import Vue from 'vue';
import endpoint from 'endpoints';
import VueStrap from 'vue-strap';
import auth from 'services/auth';
import _ from 'lodash';
import 'components/UploadAvatar/uploadAvatar';
import template from './me.html';

export default Vue.extend({
  template,

  components: {
    modal: VueStrap.modal
  },

  data() {
    return {
      profile: {
        username: '',
        photo: '',
        email: '',
        person: {
          name: ''
        }
      },
      password: {
        currentPassword: '',
        newPassword: '',
        confirmationNewPassword: ''
      },
      error: '',
      profileCheck: null,
      showConfirmClose: false,
      confirmPassword: ''
    };
  },

  created() {
    this.getMe();
  },

  watch: {
    showConfirmClose(val) {
      if (val) {
        this.errors.clear();
      }
    }
  },

  methods: {
    getMe() {
      this.$http.get(endpoint.ME).then(response => {
        const me = _.pick(response.data, ['username', 'photo', 'email', 'person.name', 'person.id']);
        this.profile = me;
        this.profileCheck = _.cloneDeep(me);
      }, error => {
        this.error = error.message;
      });
    },

    updatePersonalData(scope) {
      this.$validator.validateAll(scope).then(result => {
        if (result && this.canBeUpdated()) {
          this.buttonLoading($(this.$refs.btnPersonalData));
          this.$http.put(endpoint.ME, this.profile).then(response => {
            this.buttonLoading($(this.$refs.btnPersonalData), true);
            if (response.code === 'OK') {
              this.profileCheck = _.cloneDeep(this.profile);
              this.$notify({
                type: 'success',
                text: this.$t('success.personalDataUpdate')
              });
            }
          }, error => {
            let errorMessage = this.$t('error.personalDataUpdate');
            this.buttonLoading($(this.$refs.btnPersonalData), true);
            if (error.code === 'E_VALIDATION') {
              if (_.has(error.data, 'username') && _.find(error.data.username, { rule: 'unique' })) {
                errorMessage = this.$t('error.usernameExists', { username: this.profile.username });
                this.profile.username = this.profileCheck.username;
              }
              if (_.has(error.data, 'email') && _.find(error.data.email, { rule: 'unique' })) {
                errorMessage = this.$t('error.emailExists', { email: this.profile.email });
                this.profile.email = this.profileCheck.email;
              }
            }
            this.$notify({
              type: 'error',
              text: errorMessage
            });
          });
        }
      });
    },

    canBeUpdated() {
      return this.profileCheck.username !== this.profile.username
      || this.profileCheck.email !== this.profile.email
      || this.profileCheck.person.name !== this.profile.person.name;
    },

    buttonLoading(button, reset) {
      button.button(reset ? 'reset' : 'loading');
    },

    confirmAccountClose() {
      this.$validator.validateAll().then(result => {
        if (result) {
          this.buttonLoading($(this.$refs.btnAccountClose));
          this.$http.post(endpoint.CLOSE_ME, {
            password: this.confirmPassword
          }).then(response => {
            this.buttonLoading($(this.$refs.btnAccountClose), true);
            if (response.code === 'OK') {
              this.showConfirmClose = false;
              this.$notify({
                type: 'success',
                text: this.$t('success.closeMe')
              });
              setTimeout(() => {
                auth.logout();
              }, 4300);
            }
          }, error => {
            this.confirmPassword = '';
            this.buttonLoading($(this.$refs.btnAccountClose), true);
            this.$notify({
              type: 'error',
              text: this.$t(`error.${error.code}`)
            });
          });
        }
      });
    },

    updatePassword(scope) {
      this.$validator.validateAll(scope).then(result => {
        if (result) {
          this.buttonLoading($(this.$refs.btnPassword));
          this.$http.post(endpoint.CHANGE_PASSWORD_ME, _.omit(this.password, ['confirmationNewPassword']))
            .then(response => {
              this.buttonLoading($(this.$refs.btnPassword), true);
              if (response.code === 'OK') {
                this.$notify({
                  type: 'success',
                  text: this.$t('success.changePasswordMe')
                });
                this.clearPasswordMe(scope);
              }
            }, error => {
              this.buttonLoading($(this.$refs.btnPassword), true);
              this.$notify({
                type: 'error',
                text: this.$t((error.code === 'E_USER_NOT_FOUND') ? 'error.changePasswordMe' : 'error.undefined')
              });
              this.clearPasswordMe(scope);
            });
        }
      });
    },

    clearPasswordMe(scope) {
      this.password = {
        currentPassword: '',
        newPassword: '',
        confirmationNewPassword: ''
      };
      Vue.nextTick(() => {
        this.errors.clear(scope);
      });
    }
  },
});
