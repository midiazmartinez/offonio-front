import Vue from 'vue';
import endpoint from 'endpoints';
import _ from 'lodash';
import router from 'boot/router';
import template from './edit.html';

export default Vue.extend({
  template,

  data() {
    return {
      id: this.$route.params.id,
      media: {
        name: '',
        code: null,
        description: '',
        active: false
      },
      withoutCodes: []
    };
  },

  created() {
    this.$validator.locale = this.$i18n.locale;
    this.fetchData();
  },

  methods: {

    fetchData() {
      this.$http.all([
        this.$http.get(`${endpoint.MEDIA_TYPES}/${this.id}`),
        this.$http.get(`${endpoint.MEDIA_TYPES}?fields=code`)
      ])
        .then(this.$http.spread((media, codes) => {
          this.media = _.omit(media.data, ['createdAt', 'updatedAt']);
          this.withoutCodes = _.without(_.map(codes.data, 'code'), this.media.code);
        }, () => {
          this.$notify({
            type: 'error',
            text: this.$t('ui.notify.error')
          });
        }));
    },

    updateMedia() {
      this.$validator.validateAll();
      if (!this.errors.any()) {
        this.$emit('btn-submit-loading', true);
        this.$http.put(`${endpoint.MEDIA_TYPES}/${this.id}`, this.media).then(() => {
          this.$emit('btn-submit-loading', false);
          this.$notify({
            type: 'success',
            text: this.$t('ui.notify.successEdit', { record: this.media.name })
          });
          this.goToMediaTypes();
        }, () => {
          this.$notify({
            type: 'error',
            text: this.$t('ui.notify.errorEdit', { record: this.media.name })
          });
          this.goToMediaTypes();
        });
      }
    },

    goToMediaTypes() {
      router.push({ name: 'media-types' });
    }
  },
});
