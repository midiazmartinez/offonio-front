import Vue from 'vue';
import endpoint from 'endpoints';
import router from 'boot/router';
import _ from 'lodash';
import SelectCode from 'components/SelectCode/selectCode';
import template from './create.html';

export default Vue.extend({
  template,

  components: {
    SelectCode
  },

  data() {
    return {
      media: {
        name: '',
        code: '',
        description: ''
      },
      withoutCodes: []
    };
  },

  created() {
    this.$validator.locale = this.$i18n.locale;
    this.fetchData();
  },

  methods: {

    fetchData() {
      this.$http.get(`${endpoint.MEDIA_TYPES}?fields=code`)
        .then(codes => {
          this.withoutCodes = _.map(codes.data, 'code');
        }, () => {
          this.$notify({
            type: 'error',
            text: this.$t('ui.notify.error')
          });
        });
    },

    createMedia() {
      this.$validator.validateAll();
      if (!this.errors.any()) {
        this.$http.post(endpoint.MEDIA_TYPES, this.media).then(response => {
          if (response.code === 'CREATED') {
            this.$notify({
              type: 'success',
              text: this.$t('ui.notify.successCreate', { record: this.media.name })
            });
            this.goToMediaTypes();
          }
        }, () => {
          this.$notify({
            type: 'error',
            text: this.$t('ui.notify.errorCreate', { record: this.media.name })
          });
          this.goToMediaTypes();
        });
      }
    },

    goToMediaTypes() {
      router.push({ name: 'media-types' });
    }
  },
});
