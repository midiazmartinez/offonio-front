import Vue from 'vue';
import endpoint from 'endpoints';
import template from './searchResult.html';

export default Vue.extend({
  template,

  data() {
    return {
      result: '',
      error: '',
    };
  },

  created() {
    this.search(this.$route.params.search);
  },

  watch: {
    $route(to) {
      this.search(to.params.search);
    }
  },

  methods: {
    search(query) {
      this.$http.get(`${endpoint.SEARCH}${query}`).then(response => {
        this.result = response.data;
      }, error => {
        this.error = error.message;
      });
    }
  },
});
