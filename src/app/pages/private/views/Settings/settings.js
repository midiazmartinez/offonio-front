import Vue from 'vue';
import template from './settings.html';

export default Vue.extend({
  template,

  data() {
    return {
      error: '',
    };
  },

  created() {
  },

  methods: {
  },
});
