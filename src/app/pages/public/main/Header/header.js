import Vue from 'vue';
import auth from 'services/auth';
import 'libs/jquery.easing.min.js';
import 'libs/scrolling-nav.js';
import PublicAuthenticated from 'layoutpublicmain/Authenticated/authenticated';
import template from './header.html';

export default Vue.extend({
  template,

  components: {
    PublicAuthenticated
  },

  data() {
    return {
      user: auth.user,
      isCollapsed: true
    };
  },

  computed: {
    isEntry() {
      return this.$store.getters.isAdvertiser && !auth.user.authenticated;
    },

    isAdvertiser() {
      return this.$store.getters.isAdvertiser || auth.user.authenticated;
    }
  },

  methods: {
    logout() {
      auth.logout();
    },

    goToSignin() {
      this.$router.push('/public/signin');
    },

    goToSignup() {
      this.$router.push('/public/signup');
    }
  }
});
