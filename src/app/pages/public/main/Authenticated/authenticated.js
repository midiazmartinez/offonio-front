import Vue from 'vue';
import auth from 'services/auth';
import { Avatar } from 'vue-avatar';
import template from './authenticated.html';

export default Vue.extend({

  template,

  components: {
    avatar: Avatar
  },

  data() {
    return {
      user: auth.user,
      email: auth.email
    };
  },

  methods: {
    logout() {
      auth.logout();
    }
  }
});
