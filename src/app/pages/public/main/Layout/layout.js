import Vue from 'vue';
import PublicHeader from 'layoutpublicmain/Header/header';
import auth from 'services/auth';
import CookieLaw from 'vue-cookie-law';
import template from './layout.html';

export default Vue.extend({

  template,

  data() {
    return {
      user: auth.user
    };
  },

  components: {
    PublicHeader,
    CookieLaw
  },

  computed: {
    textRibbon() {
      return this.$store.getters.isAdvertiser ? this.$t('ui.button.btnConsumer') : this.$t('ui.button.btnAdvertiser');
    }
  },

  methods: {
    toogleAdvertiser() {
      this.$store.dispatch('toggleAdvertiser');
    }
  }
});
