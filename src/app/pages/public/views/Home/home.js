import Vue from 'vue';
import _ from 'lodash';
import auth from 'services/auth';
import 'components/Footer/footer';
import EnumApp from 'enums/app';
import template from './home.html';

export default Vue.extend({
  template,

  data() {
    return {
      code: '',
      urlPresentation: ''
    };
  },

  created() {
    this.urlPresentation = EnumApp.URL_PRESENTATIONS_LOCALES[this.$i18n.locale];
  },

  computed: {
    checkCode() {
      return !(_.trim(this.code) !== '');
    },

    isAdvertiser() {
      return this.$store.getters.isAdvertiser || auth.user.authenticated;
    }
  },

  methods: {
    goToContent() {
      this.$router.push(`/${this.code}`);
    },

    checkCodeSubmit() {
      if (!this.checkCode) {
        this.goToContent();
      }
    }
  }
});
