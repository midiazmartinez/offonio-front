import Vue from 'vue';
import template from './content.html';

export default Vue.extend({
  template,

  data() {
    return {
      code: this.$route.params.code
    };
  },

  created() {
  },

  methods: {
  },
});
