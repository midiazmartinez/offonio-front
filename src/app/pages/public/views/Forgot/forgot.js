import Vue from 'vue';
import template from './forgot.html';

export default Vue.extend({
  template,

  data() {
    return {
      username: ''
    };
  },

  methods: {
    passwordRecovery() {
      this.$validator.validateAll().then(result => {
        if (result) {
          console.log('Llamar al API para crear el link');
        }
      });
    }
  },
});
