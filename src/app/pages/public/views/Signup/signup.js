import Vue from 'vue';
import endpoint from 'endpoints';
import template from './signup.html';

export default Vue.extend({
  template,

  data() {
    return {
      user: {
        username: '',
        password: '',
        email: '',
        person: {
          name: ''
        }
      },
      confirmationPassword: '',
      succes: false,
      error: ''
    };
  },

  created() {
    this.$validator.locale = this.$i18n.locale;
  },

  methods: {
    createNewAccount() {
      this.$validator.validateAll().then(result => {
        if (result) {
          this.$http.post(endpoint.SIGNUP, this.user).then(response => {
            this.succes = (response.code === 'CREATED' && response.data);
          }, error => {
            this.error = error.message;
          });
        }
      });
    }
  }
});
