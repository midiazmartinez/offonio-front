import Vue from 'vue';

export default Vue.extend({
  template: `
    <div class="container navbar-size">
        <h1 class="page-header">Not found...</h1>
        <p class="lead">Sorry :(</p>
    </div>
  `,
});
