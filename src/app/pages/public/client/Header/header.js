import Vue from 'vue';
import auth from 'services/auth';
import 'libs/jquery.easing.min.js';
import 'libs/scrolling-nav.js';
import 'components/Language/language';
import template from './header.html';

export default Vue.extend({
  template,

  data() {
    return {
      isCollapsed: true
    };
  },

  methods: {
    logout() {
      auth.logout();
    },

    goToSignin() {
      this.$router.push('/public/signin');
    },

    goToSignup() {
      this.$router.push('/public/signup');
    }
  }
});
