import Vue from 'vue';
import PublicContentHeader from 'layoutpublicclient/Header/header';
import template from './layout.html';

export default Vue.extend({

  template,

  components: {
    PublicContentHeader
  }
});
