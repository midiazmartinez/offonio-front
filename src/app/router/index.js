// Public views
import PublicMainLayout from 'layoutpublicmain/Layout/layout';
import PublicClientLayout from 'layoutpublicclient/Layout/layout';
import Home from 'viewspublic/Home/home';
import NotFound from 'viewspublic/NotFound/notFound';
import Signin from 'viewspublic/Signin/signin';
import Signup from 'viewspublic/Signup/signup';
import Forgot from 'viewspublic/Forgot/forgot';
import Content from 'viewspublic/Content/content';
import Terms from 'viewspublic/Legal/terms';
import PrivacyPolicy from 'viewspublic/Legal/privacyPolicy';
import CookiesPolicy from 'viewspublic/Legal/cookiesPolicy';

// Administration views
import AdminLayout from 'layoutadmin/Layout/layout';
import Users from 'viewsadmin/Users/users';
import UsersEdit from 'viewsadmin/Users/edit';
import Me from 'viewsadmin/Me/me';
import Settings from 'viewsadmin/Settings/settings';
import Dashboard from 'viewsadmin/Dashboard/dashboard';
import SearchResult from 'viewsadmin/SearchResult/searchResult';
import ConsultCampaign from 'viewsadmin/Campaigns/consult';
import CreateCampaign from 'viewsadmin/Campaigns/create';
import MediaTypes from 'viewsadmin/MediaTypes/mediaTypes';
import MediaTypesCreate from 'viewsadmin/MediaTypes/create';
import MediaTypesEdit from 'viewsadmin/MediaTypes/edit';
import FormatTypes from 'viewsadmin/FormatTypes/formatTypes';
import FormatTypesCreate from 'viewsadmin/FormatTypes/create';
import FormatTypesEdit from 'viewsadmin/FormatTypes/edit';
import ContentTypes from 'viewsadmin/ContentTypes/contentTypes';
import ContentTypesCreate from 'viewsadmin/ContentTypes/create';
import ContentTypesEdit from 'viewsadmin/ContentTypes/edit';

export default [
  {
    path: '/',
    component: PublicMainLayout,
    children: [
      { path: '/', component: Home, meta: { titleKey: 'home' } },
      { path: 'notfound', component: NotFound, meta: { titleKey: 'notFound' } }
    ]
  },
  {
    path: '/public',
    component: PublicClientLayout,
    children: [
      { path: '/', component: Signin, meta: { titleKey: 'signin', entry: true } },
      { path: 'signin', component: Signin, meta: { titleKey: 'signin', entry: true } },
      { path: 'signup', component: Signup, meta: { titleKey: 'signup', entry: true } },
      { path: 'forgot', component: Forgot, meta: { titleKey: 'forgot', entry: true } },
      { path: 'terms', component: Terms, meta: { titleKey: 'terms' } },
      { path: 'privacy-policy', component: PrivacyPolicy, meta: { titleKey: 'privacyPolicy' } },
      { path: 'cookies-policy', component: CookiesPolicy, meta: { titleKey: 'cookiesPolicy' } },
      { path: '*', component: NotFound, meta: { titleKey: 'notFound' } }
    ]
  },
  {
    path: '/admin',
    component: AdminLayout,
    children: [
      { path: '/', component: Dashboard, meta: { titleKey: 'dashboard', auth: true } },
      {
        path: 'search/:search', name: 'admin-search', component: SearchResult, meta: { titleKey: 'search', super: true }
      },
      { path: 'dashboard', component: Dashboard, meta: { titleKey: 'dashboard', auth: true } },
      { path: 'me', component: Me, meta: { titleKey: 'me', auth: true } },
      { path: 'settings', component: Settings, meta: { titleKey: 'settings', auth: true } },
      { path: 'campaigns', component: ConsultCampaign, meta: { titleKey: 'campaigns', auth: true } },
      { path: 'create-campaign', component: CreateCampaign, meta: { titleKey: 'createCampaign', auth: true } },
      {
        path: 'users', name: 'admin-users', component: Users, meta: { titleKey: 'users', super: true }
      },
      { path: 'users/:id/edit', component: UsersEdit, meta: { titleKey: 'usersEdit', super: true } },
      {
        path: 'media-types', name: 'media-types', component: MediaTypes, meta: { titleKey: 'mediaTypes', super: true }
      },
      {
        path: 'media-types/create', name: 'media-types-create', component: MediaTypesCreate, meta: { titleKey: 'mediaTypesCreate', super: true }
      },
      { path: 'media-types/:id/edit', component: MediaTypesEdit, meta: { titleKey: 'mediaTypesEdit', super: true } },
      {
        path: 'format-types', name: 'format-types', component: FormatTypes, meta: { titleKey: 'formatTypes', super: true }
      },
      {
        path: 'format-types/create', name: 'format-types-create', component: FormatTypesCreate, meta: { titleKey: 'formatTypesCreate', super: true }
      },
      { path: 'format-types/:id/edit', component: FormatTypesEdit, meta: { titleKey: 'formatTypesEdit', super: true } },
      {
        path: 'content-types', name: 'content-types', component: ContentTypes, meta: { titleKey: 'contentTypes', super: true }
      },
      {
        path: 'content-types/create', name: 'content-types-create', component: ContentTypesCreate, meta: { titleKey: 'contentTypesCreate', super: true }
      },
      { path: 'content-types/:id/edit', component: ContentTypesEdit, meta: { titleKey: 'contentTypesEdit', super: true } },
      { path: '*', component: NotFound, meta: { titleKey: 'notFound' } }
    ]
  },
  { path: '/:code', component: Content, meta: { titleKey: 'content' } }
];
