export default {
  LOGIN: '/auth/signin',
  SIGNUP: '/auth/signup',
  REFRESH_TOKEN: '/auth/refresh_token',
  USERS: '/users',
  USERS_FULL: '/users/full',
  ME: '/users/me',
  AVATAR: '/users/avatar',
  CLOSE_ME: '/users/closeme',
  CHANGE_PASSWORD_ME: '/users/changepasswordme',
  ROLES: '/roles?fields=id,name&active=1&sort=name',
  MEDIA_TYPES: '/mediatypes',
  FORMAT_TYPES: '/formattypes',
  CONTENT_TYPES: '/contenttypes',
  SEARCH: '/search?q='
};
