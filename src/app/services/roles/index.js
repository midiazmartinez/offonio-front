export default {
  setRoles(currentRol) {
    return {
      isSuper: this.isRole(currentRol, 1),
      isAdmin: this.isRole(currentRol, 2),
      isGuest: this.isRole(currentRol, 3)
    };
  },

  isRole(currentRol, rol) {
    return currentRol <= rol;
  }
};
