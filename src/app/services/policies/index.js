import auth from 'services/auth';
import { store } from 'boot/initial';

export default {

  isAuthenticated(to, from, next) {
    if (auth.user.authenticated) {
      next();
    } else {
      next('/public/signin');
    }
  },

  isNotAuthenticated(to, from, next) {
    if (!auth.user.authenticated) {
      next();
    } else {
      next(from.path);
    }
  },

  isEntry(to, from, next) {
    if (store.getters.isAdvertiser && !auth.user.authenticated) {
      next();
    } else {
      next(from.path);
    }
  },

  isSuper(to, from, next) {
    if (auth.user.authenticated && auth.user.roles.isSuper) {
      next();
    } else {
      next('/admin');
    }
  },

  isAdmin(to, from, next) {
    if (auth.user.authenticated && auth.user.roles.isAdmin) {
      next();
    } else {
      next('/admin');
    }
  },

  isGuest(to, from, next) {
    if (auth.user.authenticated && auth.user.roles.isGuest) {
      next();
    } else {
      next('/admin');
    }
  },
};
